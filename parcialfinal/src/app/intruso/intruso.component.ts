import { Component, OnInit } from '@angular/core';
import {Servicio1Service} from '../servicio1.service';

@Component({
  selector: 'app-intruso',
  templateUrl: './intruso.component.html',
  styleUrls: ['./intruso.component.css']
})
export class IntrusoComponent implements OnInit {

  constructor(private _Servicio: Servicio1Service) { }

  ngOnInit(): void {
  }

  pausar() {
    this._Servicio.establecerEstado(0);
  }
}
