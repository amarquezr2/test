import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Servicio1Service {
  private contador: any;
  constructor() { }

  leerEstado()
  {
    return this.contador;
  }

  establecerEstado(valor: number)
  {
    this.contador = valor;
  }
}
