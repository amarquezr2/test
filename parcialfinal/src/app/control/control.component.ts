import { Component, OnInit } from '@angular/core';
import {Servicio1Service} from '../servicio1.service';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit {

  // tslint:disable-next-line:variable-name
  constructor(private _Servicio: Servicio1Service) { }

  ngOnInit(): void {
  }

  iniciar() {
    let servicio = this._Servicio;
    servicio.establecerEstado(12);

    let test = this._Servicio.leerEstado();
    while(test > 0)
    {
      setTimeout(function(){
      },50000);
      console.log("corriendo");
      test = this._Servicio.leerEstado();
    }
    if(test == 0)
    {
      alert("partida detenida!!!!");
    }
  }
}
