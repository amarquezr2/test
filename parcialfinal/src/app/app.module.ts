import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ControlComponent } from './control/control.component';
import { IntrusoComponent } from './intruso/intruso.component';
import { CambioComponent } from './cambio/cambio.component';
import { Servicio1Service } from './servicio1.service';

@NgModule({
  declarations: [
    AppComponent,
    ControlComponent,
    IntrusoComponent,
    CambioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [Servicio1Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
